const db = require("../configs/db");

exports.getProduct = (res, state) => {
    db.query(state, (err, rows) => {
      if (err) {
        return res.status(404).json({
          message: "Data product gagal diambil!",
          error: err,
        });
      } else {
        return res.status(200).json({
          message: "Data product berhasil diambil cuy!",
          data: rows,
        });
      }
    });
  };