const {
  getProduct,
  deleteUser,
  updateUser,
  insertUser,
} = require("../models/productModel");

exports.getProduct = (req, res) => {
  const querySql = "SELECT * FROM products";
  getProduct(res, querySql);
};

exports.getProductByID = (req, res) => {
  const querySql = `SELECT * FROM products WHERE id=${req.params.id}`;
  getProduct(res, querySql);
};
