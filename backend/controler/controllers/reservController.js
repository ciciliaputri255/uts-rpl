const {
  getReserv,
  deleteReserv,
  updateReserv,
  insertReserv,
} = require("../models/reservModel");

exports.getAllReserv = (req, res) => {
  const querySql = "SELECT * FROM reservasi";
  getReserv(res, querySql);
};

exports.getReservByUser = (req, res) => {
  const id = req.params.id;
  const querySql = `SELECT * FROM reservasi WHERE id_user = ${id}`;
  getReserv(res, querySql);
};

exports.updateReserv = (req, res) => {
  const data = { ...req.body };
  const querySearch = "SELECT * FROM reservasi WHERE id_reservasi = ?";
  const queryUpdate = "UPDATE reservasi SET ? WHERE id_reservasi = ?";
  updateReserv(res, querySearch, queryUpdate, req.params.id, data);
};

exports.createReserv = (req, res) => {
  const data = { ...req.body };
  const querySql = "INSERT INTO reservasi SET ?";
  insertReserv(res, querySql, data);
};

exports.deleteReserv = (req, res) => {
  const querySearch = "SELECT * FROM reservasi WHERE id_reservasi = ?";
  const queryDelete = "DELETE FROM reservasi WHERE id_reservasi = ?";
  deleteReserv(res, querySearch, queryDelete, parseInt(req.params.id));
};
