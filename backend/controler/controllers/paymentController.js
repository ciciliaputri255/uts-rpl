const {
  getPayment,
  deletePayment,
  updatePayment,
  insertPayment,
} = require("../models/PaymentModel");
const { authenticateGoogle, uploadToGoogleDrive } = require("../services/googleDriveServices");
const fs = require("fs");

exports.getAllPayment = (req, res) => {
  const querySql = "SELECT * FROM payment";
  getPayment(res, querySql);
};

exports.getPaymentByUser = (req, res) => {
  const id = req.params.id;
  const querySql = `SELECT * FROM payment WHERE id_user = ${id}`;
  getPayment(res, querySql);
};

exports.updatePayment = (req, res) => {
  const data = { ...req.body };
  const querySearch = "SELECT * FROM payment WHERE id = ?";
  const queryUpdate = "UPDATE payment SET ? WHERE id = ?";
  updatePayment(res, querySearch, queryUpdate, req.params.id, data);
};

exports.createPayment = (req, res) => {
  const data = { ...req.body };
  const querySql = "INSERT INTO payment SET ?";
  insertPayment(res, querySql, data);
};

exports.deletePayment = (req, res) => {
  const querySearch = "SELECT * FROM payment WHERE id = ?";
  const queryDelete = "DELETE FROM payment WHERE id = ?";
  deletePayment(res, querySearch, queryDelete, parseInt(req.params.id));
};

const deleteFile = (filePath) => {
  fs.unlink(filePath, () => {
    console.log("file deleted");
  });
};

exports.uploadFile = async (req, res, next) => {
  try {
    if (!req.file) {
      res.status(400).send("No file uploaded.");
      return;
    }
    const auth = authenticateGoogle();
    const response = await uploadToGoogleDrive(req.file, auth);
    deleteFile(req.file.path);
    res.status(200).json({ response });
  } catch (err) {
    console.log(err);
  }
};
